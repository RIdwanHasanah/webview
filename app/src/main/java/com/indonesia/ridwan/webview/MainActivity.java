package com.indonesia.ridwan.webview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        WebView mayWebView = (WebView) findViewById(R.id.web);
        mayWebView.setWebViewClient(new WebViewClient());
        mayWebView.getSettings().setJavaScriptEnabled(true);
        mayWebView.getSettings().setDomStorageEnabled(true);

        WebSettings webSettings = mayWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        mayWebView.loadUrl("http://students.bsi.ac.id/mahasiswa/login.aspx");

        //Untuk membuat vitur zoom
        mayWebView.getSettings().setBuiltInZoomControls(true);
        mayWebView.getSettings().setSupportZoom(true);
    }
}
